const express = require('express');
const router = express.Router();



router.get('/', (req, res) => {

  res.format({
    html: function () {
      res.send('Accept: text/html here');
    },

    json: function () {
      var file = {
        body: "Successful the JSON Request!"
      }
      res.jsonp(file);
    }
  })

});

module.exports = router;