const express = require('express');
const router = express.Router();

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('Developed with love, at <a href="http://alia.ml">ΛLIΛ<a> & <a href="http://intuite.ch">Intuitech</a>');
});

module.exports = router;