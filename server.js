'use strict'
// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const compression = require('compression');
const router = express.Router();
// Get our API routes
const api = require('./server/routes/api');
const json = require('./server/routes/pjson');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(compression());

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));
app.use(express.static(path.join(__dirname, 'server/routes/texts.json')));

// Set our api routes
app.use('/api', api);
app.use('/json', json);

// Catch all other routes and return the index file
router.get('*', (req, res) => {
  res.format({
    html: function () {
      res.sendFile(path.join(__dirname, 'dist/index.html'));
    },
    json: function () {
      file = {
        body: "this is index json response"
      }
      res.json(file);
    }
  })
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '9200';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));