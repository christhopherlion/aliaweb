// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB49pWKjhnW4UZNLFuQfldjOLCnEigeYF8",
    authDomain: "alia-code.firebaseapp.com",
    databaseURL: "https://alia-code.firebaseio.com",
    projectId: "alia-code",
    storageBucket: "alia-code.appspot.com",
    messagingSenderId: "893083858153"
  }
};
