export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyB49pWKjhnW4UZNLFuQfldjOLCnEigeYF8",
    authDomain: "alia-code.firebaseapp.com",
    databaseURL: "https://alia-code.firebaseio.com",
    projectId: "alia-code",
    storageBucket: "alia-code.appspot.com",
    messagingSenderId: "893083858153"
  }
};
