import { Component, OnInit } from '@angular/core';
import { ToasterService, ToasterConfig, Toast } from 'angular2-toaster';

@Component({
  selector: 'notification',
  providers: [ToasterService],
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent {
  private toasterService: ToasterService;

  constructor(toasterService: ToasterService) {
    this.toasterService = toasterService;
  }
  public toasterconfig: ToasterConfig =
  new ToasterConfig({
    showCloseButton: true,
    tapToDismiss: true,
    animation: 'fade',
    timeout: 0
  });
  ngAfterViewInit() {
    let toast: Toast = {
      type: 'info', data: 1,
      title: 'Under development | Em desenvolvimento',
      body: 'Im deeply sorry, but this site still being coded. come back later',
      showCloseButton: true,
      timeout: 12000,
      onHideCallback: (toast) => console.log(toast.data)
    };
    this.toasterService.pop(toast);
  }

}
