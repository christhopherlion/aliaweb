import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.scss']
})
export class SkillListComponent {

  design: Observable<any[]>;
  front: Observable<any[]>;
  backend: Observable<any[]>;
  constructor(db: AngularFireDatabase) {
    this.design = db.list('/tools/design').valueChanges();
    this.front = db.list('/tools/front').valueChanges();
    this.backend = db.list('/tools/backend').valueChanges();
  }

}
