import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'alia',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  texts: Observable<any>;
  constructor(db: AngularFireDatabase) {
    this.texts = db.object('/pt').valueChanges();
  }

}
