import { Component } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent extends AppComponent {

}
